(use '[clojure.set :only (union)])
(use '[clojure.string :only (join split blank?)])

(defn split-input
  [input]
  (concat '(" " " ")
          (seq (split input #" "))
          '(" " " ")))

(defn threegrams
  [input]
  (if (< (count input) 3)
    nil
    (lazy-seq (cons {(join " " (take 2 input)) [(nth input 2)]}
                    (threegrams (rest input))))))

(defn chain-gen
  [input]
  (reduce #(merge-with union %1 %2)
          (threegrams (split-input input))))

(defn next-word
  [chain words]
  (let [c (count words)]
    (rand-nth (chain (join " " (take-last 2 words))))))

(defn create-seq
  [chain words]
  (let [nw (next-word chain words)]
    (cons nw (lazy-seq (create-seq chain (concat words [nw]))))))

(defn stringiffy
  [words]
  (join " " (filter #(not (blank? %)) words)))

(defn generate
  [input length]
  (let [chain (chain-gen input)]
    (stringiffy (take length (create-seq chain (split (rand-nth (keys chain)) #" "))))))

    (defn get-line
  [filename]
  (with-open [r (clojure.java.io/reader filename)]
     (doall (line-seq r))))

(defn list-of-words
  [fname]
  (filter #(not (clojure.string/blank? %)) (clojure.string/split (slurp fname) #" |\n|\r")))

(defn markov-chain
  [filename length]
  (generate (clojure.string/trim-newline (clojure.string/lower-case (get-line filename))) length))

(defn main
  [filename length]
  (generate (clojure.string/join " " (list-of-words filename))  length))
